#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>

int votes = 0;
int pshared;
int value;
sem_t semmutex;		//declare semaphore var to replace mutex

void *countvotes(void *param) {
  int i;
  // printf("In function \nthread id = %d\n", pthread_self());
  for (i = 0; i < 10000000; i++) {
    	sem_wait(&semmutex);
    	votes += 1;
        sem_post(&semmutex);
  }
    pthread_exit(NULL);
  return NULL;
}

int main() {

 pthread_t tid1, tid2, tid3, tid4, tid5;

sem_init(&semmutex,0,1);  // intialize the semaphore variable declared,see notation below as to why 0 or 1 in pshared and initialzing value to 1 this is binary sempahore

  pthread_create(&tid1, NULL, countvotes, NULL);
  //printf("Thread: %s \n", (pthread_t));
  pthread_create(&tid2, NULL, countvotes, NULL);
  //printf("Thread 2 \n");
  pthread_create(&tid3, NULL, countvotes, NULL);
  //printf("Thread 3 \n");
  pthread_create(&tid4, NULL, countvotes, NULL);
  //printf("Thread 4 \n");
  pthread_create(&tid5, NULL, countvotes, NULL);
  //printf("Thread 5 \n");

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);
  pthread_join(tid3, NULL);
  pthread_join(tid4, NULL);
  pthread_join(tid5, NULL);
 
 sem_destroy(&semmutex);

  printf("Vote total is %d\n", votes);
  return 0;
}

/*function initializes a semaphore object pointed to by sem, sets its sharing option and gives it an initial integer value. The pshared parameter controls the type of semaphore. If the value of pshared is 0, the semaphore is local to the current process. Otherwise, the semaphore may be shared between processes. Here we are interested only in semaphores that are not shared between processes int sem_init(sem_t *sem, int pshared, unsigned int value);*/

//int ret = sem_init(&semmutex, pshared, value); //takes a pointer to the semaphore object initialized by a call to sem_init.

//int sem_wait(sem_t * semmutex);  //atomically decreases value of semaphore by one, but waits until the semaphore has a nonzero count first

//int sem_post(sem_t * sem);  //atomically increases the value of the semaphore by 1

//int sem_getvalue(sem_t* sem, int* sval);
//int sem_destroy(sem_t * sem);

