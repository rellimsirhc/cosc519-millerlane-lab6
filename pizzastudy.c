#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <stdbool.h> 
#include <unistd.h>   //To ensure concurrent execution, use the sleep() function from unistd.h to mimic a studying or napping student.

void sleeping(int);
void studyeat (int,int);
void delivery();
void student(void *ptr);
void ordered (int,int);

// Going to sleep until the sleep function returns
//sleep(rand() % 3);
// Guh! I’m awake!

sem_t semaphore;
sem_t mutex; 

int slices = 0;  // num of slices assume 8 slices per pizza
int order =0;  //did a pizza get ordered 
int orderno = 0, pies = 0; //number orders

int main (int argc, char *argv[]){

pthread_t tid1, tid2, tid3, tid4, tid5, tidp;   // include t
int thredarry[6] = {0,1,2,3,4,5};;  //was 
//int pthread_t tid[4]; 	//id pthread array
//pthread_attr_t attr; 	//set or thread attributes  	 


int n = atoi(argv[1]);  //number of threads to be created   
				//int n = *(int *)argv;	
//code should take in a single runtime argument representing the number of students in study group and should be between 2-5.				
	while( n < 2 || n > 5)   
	   {
	   printf("\n Please choose between 2 and 5 students for study group.\n");
	   fflush(stdin);
	   scanf ("%d", &n);
	    }
sem_init(&mutex,0,1);  // intialize the semaphore variable declared,see notation below as to why 0 or 1 in pshared and initialzing value to 1 this is binary sempahore	    
sem_init(&semaphore,0,0);  // intialize the semaphore variable declared,see notation below as to why 0 or 1 in pshared and initialzing value to 1 this is binary sempahore

//while (orderno <=13){

  pthread_create(&tidp, NULL, (void *) &delivery, (void *) &thredarry[0]);
 printf("Thread Deliver Pizza \n");
 
 
 /*int ptr[4];  
    	 for (int i = 1; i <= n; i++){
  		ptr[i-1] = i*1;  	//ptr array to ensure unique ptr to pass to worker
	 	pthread_create(&tid[i-1],NULL,&student,&ptr[i-1]); //(void *)thread creation &is address of operator
		printf("Creating Thread %d\n",i);
	}
	  for (int i = 1; i <= n; i++){
	  	pthread_join(tid[i-1], NULL);    // main thread wait on others
	   	printf("\n");
	  exit(0);
	 }*/
	 
 pthread_create(&tid1, NULL, (void *) &student, (void *) &thredarry[1]);
 // printf("Thread stud 1: %s \n");
  pthread_create(&tid2, NULL,  (void *) &student, (void *) &thredarry[2]);
  //printf("Thread stud 2 \n");
  pthread_create(&tid3, NULL, (void *) &student, (void *) &thredarry[3]);
  //printf("Thread student 3 \n");
  pthread_create(&tid4, NULL, (void *) &student, (void *) &thredarry[4]);
  //printf("Thread student 4 \n");
  pthread_create(&tid5, NULL,  (void *) &student, (void *) &thredarry[5]);
  //printf("Thread student 5 \n");
    
  pthread_join(tidp, NULL);
  
 pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);
  pthread_join(tid3, NULL);
  pthread_join(tid4, NULL);
  pthread_join(tid5, NULL);
    
 sem_destroy(&mutex);
 sem_destroy(&semaphore);
//}
return 0;
}
void student(void *ptr)
{
 	/*int idt = *(int *)arg;
  	 printf("Hello World! I am thread # %d\n", idt);
	//pthread_exit(0); //terminate the threads*/

    while(1)
    {
        int sid = *((int *) ptr);  //  (int *) &var, takes a pointer to a var, converts it to a pointer of int type, so (int *) is a type specific pointer, leads to (type)x as a type cast
        
        sem_wait(&mutex);     //acquire semaphore, no two students get slice at same time or eat same slice AND no two students to order at same time protect slices and ordering
        
 /*while(true){
	pick up a slice of pizza
	study while eating pizza slice
    }*/
  
        if( slices > 0 ){
            slices--;
            sem_post(&mutex);   //release semaphore
            studyeat(sid,slices+1);
        }
        else{
            if(order == 0 && pies < 13)
            {
                order++;
                pies++;
                //ordered(sid,pies+1);
                printf("student%d is ordering pizza %d.\n",sid, pies);
                sem_post(&semaphore);
              }    
            sem_post(&mutex);  
            sleeping(sid);
        }
    }
}
void studyeat (int student,int slice)  // student is eating a slice of pizza and studying
{
         //while slices available eat & study (move code above here)

    printf("student%d studying and eating slice %d\n",student,slice);

}

void sleeping(int student)		//student sleeping
{
    printf("student%d is sleeping\n",student);
    sleep(1);
}

void ordered (int student,int pie)  // student ordered pizza
{
    //move order code here from above
    
    printf("student%d ordered pizza %d\n",student,pie);
}

void delivery(){
    while(1)
    {
        sem_wait(&semaphore);
        sem_wait(&mutex);
 //      if (orderno < 13){
        printf("Seasons pizza recived and order for a pizza.\n");
        slices += 8; // 8 slices per pizza.  //value of the expression to the right of the operator is added to the value of the variable to the left of the operator, and the result replaces the value of the variable
      orderno +=1;
       printf ("Pizza number %d delivered \n", orderno);
        order = 0; // reset the order.
        sem_post(&mutex);
   //     }
    }
}

